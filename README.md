# READ-ME

The `findExclusiveReplacements.py` script his is a simple Python v3+ program that extract unique replacements from a multiple sequences alignment.

## Dependencies

- [Python v3+](https://www.python.org/)
- [Biopython](https://biopython.org/)

## Usage

Excute as `findExclusiveReplacements.py [-h] -f FASTA -i INGROUP [-m MUTATIONS] [-o OUTPUT] -r REFERENCE`.

## Optional arguments:

- -`h` or `--help`: show help message

- `-f` or `--fasta`: Multi-FASTA file with aligned sequences

- `-i` or `--ingroup`: Simple text file listing the ingroup sequence IDs, one per line. Sequence IDs must match the alignment.

- `-m` or `--mutations`: Optional. Simple text with list of target mutations, one per line. Target mutations must be written as character state in the reference plus position in the reference plus character state in the target sequence (for example: N1584T).

-  `-o` or `--output`: Optional (default = "results"). Prefix for output files.

-  `-r` or `--reference`: Sequence ID of the reference sequence.

## List of example files

- `mutations.txt`: List of target mutations, one per line. Empty lines and comments starting with pound signs are ignored.
- `alignment.pep`: Multifasta file with amino acid alignment. Note that the file extension is not important.
- `ingroup.txt`: List of target sequences (i.e., ingroup sequences). This does not need to be a natural group.

## How to execute using example files

Execute the main script using the example files as follows:

```bash
python3 findExclusiveReplacements.py -f examples/alignment.pep -m examples/mutations.txt -i examples/ingroup.txt -r SCN4A_RAT -o example
```
