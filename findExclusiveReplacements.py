#!/usr/bin/env python3

# Import your stuff
import argparse, re, sys
from Bio import SeqIO

# Define your arguments
parser = argparse.ArgumentParser()
parser.add_argument("-f", "--fasta", type=str, help = "Multi-FASTA file with aligned sequences", required = True)
parser.add_argument("-i", "--ingroup",  type=str, help = "File listing the ingroup sequence IDs, one per line", required = True)
parser.add_argument("-m", "--mutations", type=str, help = "List of target mutations", default = "none", required = False)
parser.add_argument("-o", "--output", type=str, help = "Prefix for output files", default = "results", required = False)
parser.add_argument("-r", "--reference", type=str, help = "Sequence ID of the reference sequence", required = True)
args = parser.parse_args()

# Define functions
def main():
	handle1 = open("{}_unique.tsv".format(args.output), "w")
	if args.mutations != "none":
		handle2 = open("{}_selected.tsv".format(args.output), "w")
	ingroup = get_ingroup_sequences()
	reference = args.reference
	alignment = get_alignment_data()
	check_alignment(alignment, reference, ingroup)
	ref_positions, ref_values = parse_reference(alignment[reference])
	total = len(ref_positions)
	ingroup_values = {}
	outgroup_values = {}
	for s in range(0, len(ref_values)):
		ingroup_values[s] = []
		outgroup_values[s] = []
	for seqid in ingroup:
		ingroup_values = parse_alignment(ingroup_values, alignment[seqid], total)
	for s in range(0, total):
		ingroup_values[s] = set(ingroup_values[s])
	outgroup = []
	for seqid in alignment:
		if not seqid in ingroup:
			outgroup.append(seqid)
	for seqid in outgroup:
		outgroup_values = parse_alignment(outgroup_values, alignment[seqid], total)
	for s in range(0, total):
		outgroup_values[s] = set(outgroup_values[s])
	ingroup_exclusive = {}
	for s in range(0, total):
		ingroup_set = ingroup_values[s]
		outgroup_set = outgroup_values[s]
		x = ingroup_set - outgroup_set
		if x:
			ingroup_exclusive[s] = x
	sys.stdout.write("seqids\treference\tingroup\tposition\tstring\n")
	handle1.write("seqids\treference\tingroup\tposition\tstring\n")
	for s in ingroup_exclusive:
		ref_state = ref_values[s]
		ingroup_state = ingroup_exclusive[s]
		p = ref_positions[s]
		hits = []
		for current_state in ingroup_state:
			for seqid in ingroup:
				if alignment[seqid][s] == current_state:
					hits.append(seqid)
		hits = ",".join(list(set(hits)))
		sys.stdout.write("{}\t{}\t{}\t{}\t{}\n".format(hits, ref_state, ",".join(list(ingroup_state)), p, s))
		handle1.write("{}\t{}\t{}\t{}\t{}\n".format(hits, ref_state, ",".join(list(ingroup_state)), p, s))
	if args.mutations != "none":
		mutations = parse_mutations(args.mutations)
		mhits = {}
		for m in mutations:
			mhits[m] = []
			ref_state = mutations[m][0]
			p = int(mutations[m][1])
			for s in sorted(ref_positions):
				if p == ref_positions[s]:
					break
			ingroup_state = mutations[m][2]
			if ref_values[s] != ref_state:
				sys.stderr.write("! Error: did not find expected value in the reference ({})\n".format(m))
				exit()
			for seqid in alignment:
				if alignment[seqid][s] == ingroup_state:
					mhits[m] += [seqid]
		sys.stderr.write("mutation\tseqid\tgroup\n")
		handle2.write("mutation\tseqid\tgroup\n")
		for m in mhits:
			for seqid in mhits[m]:
				if seqid in ingroup:
					group = "ingroup"
				else:
					group = "outgroup"
				sys.stderr.write("{}\t{}\t{}\n".format(m, seqid, group))
				handle2.write("{}\t{}\t{}\n".format(m, seqid, group))
	handle1.close()
	if args.mutations != "none":
		handle2.close()

	return

def parse_mutations(filePath):
	mutations = {}
	handle = open(filePath, "r")
	for line in handle.readlines():
		line = re.sub("#.*", "", line)
		line = line.strip()
		if line:
			ref_state, p, ingroup_state = re.compile("(.+?)(\d+)(.+)").findall(line)[0]
			mutations[line] = [ref_state, p, ingroup_state]
	handle.close()
	return mutations

def parse_alignment(dic, seq, total):
	for s in range(0, total):
		c = seq[s]
		dic[s] += [c]
	return dic

def parse_reference(refseq):
	ref_positions = {}
	ref_values = {}
	total = len(refseq)
	p = 0
	for s in range(0, total):
		c = refseq[s]
		if c in ["-", "*", " ", "–", "—"]:
			ref_positions[s] = p
		else:
			p += 1
			ref_positions[s] = p
		ref_values[s] = c
	return ref_positions, ref_values

def check_alignment(alignment, reference, ingroup):
	if not reference in alignment:
		sys.stderr.write("! Error: reference ({}) not in alignment\n".format(reference))
		exit()
	for seqid in ingroup:
		if not seqid in alignment:
			sys.stderr.write("! Error: ingroup sequence ({}) not in alignment\n".format(reference))
			exit()
	if len(set([len(alignment[seqid]) for seqid in alignment])) !=1:
		sys.stderr.write("! Error: sequences are not of the same length\n")
		exit()
	return

def get_alignment_data():
	alignment = {}
	for record in SeqIO.parse(args.fasta, "fasta"):
		alignment[record.id] = str(record.seq)
	return alignment

def get_ingroup_sequences():
	ingroup = []
	handle = open(args.ingroup, "r")
	for line in handle.readlines():
		line = line.strip()
		if line:
			ingroup.append(line)
	handle.close()
	return ingroup

# Execute functions
main()

# Quit
exit()
